const fs = require("fs")
const path = require('path')

const { pipeline, Transform } = require("stream");

const FILE_NAME = `dig_data_file.txt`
const OUTPUT_NAME = `result.txt`

const inputFile = path.resolve("data", FILE_NAME)
const outputFile = path.resolve("data", OUTPUT_NAME)

if (fs.existsSync(outputFile)) {
    fs.unlinkSync(outputFile)
}

const readStream = fs.createReadStream(inputFile, { highWaterMark: 1 })

const filterNonWorldSymbols = new Transform({
    transform(chunk, encoding, callback) {
        const regex = /\W/ig;
        let normalizeChunk = chunk.toString().replace(regex, ' ')
        callback(null, normalizeChunk)
    },
});


class WordCounter extends Transform {

    cashedData = ``

    constructor(options) {
        super(options)
        this.wordMap = new Map()
        this.on(`finish`, () => {
            const keysAsArray = []
            for (let key of this.wordMap.keys()) {
                keysAsArray.push(key)
            }
            keysAsArray.sort()
            console.log(keysAsArray.map(i => this.wordMap.get(i)))
        })

    }

    _transform(chunk, encoding, callback) {
        const newChunk = `${this.cashedData}${chunk.toString()}`.replace(/\s{2,}/g, " ")
        const lastSeparatorIndex = newChunk.lastIndexOf(" ")
        if (lastSeparatorIndex === -1) {
            this.cashedData = newChunk
        } else {
            this.cashedData = newChunk.slice(lastSeparatorIndex + 1)
            newChunk.slice(0, lastSeparatorIndex).split(` `)
                .filter(i => i !== ``)
                .forEach(i => {
                    const count = this.wordMap.has(i) ? this.wordMap.get(i) + 1 : 1
                    this.wordMap.set(i, count)
                })
        }

        callback(null)
    }

    _flush(cb) {
        try {
            if (this.cashedData.trim()) {
                const count = this.wordMap.has(this.cashedData) ? this.wordMap.get(this.cashedData) + 1 : 1
                this.wordMap.set(this.cashedData, count)
            }
            cb()
        } catch (e) {
            cb(e)
        }
    }

}

const wordCounter = new WordCounter({ readableObjectMode: true, writableObjectMode: true })

pipeline(
    readStream,
    filterNonWorldSymbols,
    wordCounter,
    (err) => {
        if (err) {
            console.error("An error occured in pipeline.", err);
        } else {
            console.log("Pipeline execcution successful");
        }
    }
)