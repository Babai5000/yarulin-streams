const fs = require('fs')
const path = require('path')
const getRandomLengthString = require('./utils/getRandomLengthString')


const FILE_NAME = "dig_data_file.txt"
//const MAX_FILE_SIZE = 52428800
const MAX_FILE_SIZE = 1048576

const outputFile = path.resolve("data", FILE_NAME)

if (fs.existsSync(outputFile)) {
    fs.unlinkSync(outputFile)
}
let lengthData = 0
try{
    const file  = fs.createWriteStream(outputFile)
    while (lengthData <= MAX_FILE_SIZE) {
        const randomStr = getRandomLengthString()
        const buf = Buffer.from(randomStr, 'utf8')
        lengthData += buf.length
        file.write(buf);
    }

    file.end()
} catch (e){
    console.error(e)
}


