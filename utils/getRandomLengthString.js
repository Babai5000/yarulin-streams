const getRandomInt = require("./getRandomInt")

//const CHARS = `abcdefghijklmnopqrstuvwxyz0123456789`

const CHARS = `abcde`

const SEPARATORS = [` `, `,`]

module.exports = function getRandomLengthString() {
    const countWorld = getRandomInt(4, 10)

    let randomStr = ``
    for (let i = 1; i <= countWorld; i++) {
        const separator = SEPARATORS[getRandomInt(0, 1)]
        const countCharsInWorld = getRandomInt(1, 4)
        for (let j = 1; j <= countCharsInWorld; j++) {
            randomStr += CHARS[getRandomInt(0, CHARS.length - 1)]
        }
        randomStr += separator
    }
    

    return randomStr
}